﻿using System.IO;
using System.Runtime.InteropServices;
using System.Text;

////
// Credits to BLaZiNiX @ http://www.codeproject.com/
// http://www.codeproject.com/Articles/1966/An-INI-file-handling-class-using-C
//// Note: Slightly edited by myself.
namespace Desktop_manager.Misc
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string Path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="IniPath"></PARAM>
        public IniFile(string iniPath)
        {
            Path = iniPath;

        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="section"></PARAM>
        /// Section name
        /// <PARAM name="key"></PARAM>
        /// Key Name
        /// <PARAM name="value"></PARAM>
        /// Value Name
        public void IniWriteValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, this.Path);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="section"></PARAM>
        /// <PARAM name="key"></PARAM>
        /// <PARAM name="defaultValue">If no key found, it will make it with this value</PARAM>
        /// <returns></returns>
        public string IniReadValue(string section, string key, string defaultValue = "")
        {
            var temp = new StringBuilder(255);
            GetPrivateProfileString(section, key, "", temp, 255, this.Path);
            if(temp.Length == 0 && defaultValue.Length > 0)
                IniWriteValue(section, key, defaultValue);

            return temp.ToString();

        }
    }
}