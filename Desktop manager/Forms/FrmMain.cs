﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Desktop_manager.Desktop;

namespace Desktop_manager.Forms
{
    public partial class FrmMain : Form
    {
        private readonly DesktopManager _manager;

        public FrmMain()
        {
            InitializeComponent();

            _manager = new DesktopManager();
            _manager.SetDesktop(1);

            var hook = new KeyboardHook();
            hook.KeyPressed += hook_KeyPressed;

            hook.RegisterHotKey(global::ModifierKeys.Control, Keys.PageDown);
            hook.RegisterHotKey(global::ModifierKeys.Control, Keys.PageUp);

            _manager.SetDesktop(0);
            SetIcon();
        }

        void hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            switch (e.Key)
            {
                case Keys.PageDown:
                    _manager.SetDesktop(_manager.CurrentDesktopId -1);
                    break;
                case Keys.PageUp:
                    _manager.SetDesktop(_manager.CurrentDesktopId + 1);
                    break;
            }

            SetIcon();
        }

        private void SetIcon()
        {
            using (var b = new Bitmap(16, 16))
            using (var g = Graphics.FromImage(b))
            {
                g.DrawString(_manager.CurrentDesktopId.ToString(), new Font("Arial", 12), Brushes.CornflowerBlue, 
                    b.Width / 2, b.Height/ 2, new StringFormat{Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center});
                
                ntyMain.Icon = Icon.FromHandle(b.GetHicon());
            }

            ntyMain.Text = "Current desktop: " + _manager.CurrentDesktopId;
        }

        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(false);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Process.Start(_manager.GetCurrentDesktopPath());
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _manager.SetDesktop(0);

            Environment.Exit(0);
        }
    }
}
