﻿using System;
using System.Runtime.InteropServices;

////
// Credits to mixed people @ http://www.daniweb.com/
// http://www.daniweb.com/software-development/csharp/threads/334988/programmatically-changing-the-location-of-the-desktop-shell-folder
//// Note: I made some changes
namespace Desktop_manager.Desktop
{
    public static class Desktop
    {
        [DllImport("Shell32.dll")]
        public static extern int SHChangeNotify(int eventId, int flags, IntPtr item1, IntPtr item2);

        [DllImport("shell32.dll")]
        private extern static int SHSetKnownFolderPath(ref Guid folderId, uint flags, IntPtr token, [MarshalAs(UnmanagedType.LPWStr)] string path);

        public static Guid DesktopGuid = new Guid("B4BFCC3A-DB2C-424C-B029-7FE99A87C641");

        public static void ApplyNewDesktopPath(string newDesktop)
        {
            SHSetKnownFolderPath(ref DesktopGuid, 0, IntPtr.Zero, newDesktop);
            RefreshDesktop();
        }

        public static void RefreshDesktop()
        {
            SHChangeNotify(0x8000000, 0x1000, IntPtr.Zero, IntPtr.Zero);
        }
    }
}
