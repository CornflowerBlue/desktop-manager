﻿using System;
using System.IO;
using Desktop_manager.Misc;

namespace Desktop_manager.Desktop
{
    public sealed class DesktopManager
    {
        public string DesktopFolderPath;

        public int CurrentDesktopId { get; private set; }

        private readonly string DesktopDefaultFolderPath;

        public DesktopManager()
        {
            DesktopFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DesktopManager");
            if (!Directory.Exists(DesktopFolderPath))
            {
                Directory.CreateDirectory(DesktopFolderPath);
            }

            var settings = new IniFile(DesktopFolderPath + "\\Settings.ini");

            DesktopDefaultFolderPath = settings.IniReadValue("Desktop", "Path", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
        }

        private string CreateDesktopPath(int desktopId)
        {
            if (desktopId == 0) return DesktopDefaultFolderPath;

            var newPath = Path.Combine(DesktopFolderPath, "Desktop_" + desktopId);

            if (!Directory.Exists(newPath))
                Directory.CreateDirectory(newPath);

            return Path.Combine(DesktopFolderPath, "Desktop_" + desktopId);
        }

        public void SetDesktop(int desktopId)
        {
            if (desktopId < 0) desktopId = 0;
            if (desktopId > 99) desktopId = 99;

            Desktop.ApplyNewDesktopPath(CreateDesktopPath(desktopId));

            CurrentDesktopId = desktopId;
        }

        public string GetCurrentDesktopPath()
        {
            return CreateDesktopPath(CurrentDesktopId);
        }
    }
}
